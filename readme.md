# как запускать
Устанавливаем docker на свой компьютер.

https://www.docker.com/products/docker-desktop/

Запускаем Docker 

После установки Docker открываем IDE корень проекта GRUZ

Далее в IDE открываем терминал и переходим в дирректорию docker

    cd docker

Стягиваем с docker hub образы если docker-compose обновился обновляем этой коммандой. 

    docker compose -f docker-compose.dev.yaml pull

Сборка проекта режим разработчика

    docker compose -f docker-compose.dev.yaml build

После сборки проекта rebild можно не делать если не прилетела новая версия

После успешного build запускаем систему (систему можно запустить в двух режимах demon/intractive)

Запуск в режиме intractive. Плюсы данного запуска, мы видим логи контейнеров и их состояния, минусы надо держать терминал открытым.

    docker compose -f docker-compose.dev.yaml up

Запуск в режиме demon. 

Запуск в режиме demon подразумивает, что у нас все хорошо со сборкой, и мы редактируем код не меняя структуру системнызх файлов

    docker compose -f docker-compose.dev.yaml up -d

Остановка проекта 

    docker compose -f docker-compose.dev.yaml down


## PS

Для контгейнера flusk добавить переменные - не решено 

Фаил requments.txt жирный но содержит весь базис
