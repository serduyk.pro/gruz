from flask import Flask
from app.__init__ import Config
from flask_sqlalchemy import SQLAlchemy

# Создаем экземпляр Flask приложения
app = Flask(__name__)

# Применяем конфигурацию к приложению
app.config.from_object(Config)

# Инициализация SQLAlchemy после создания Flask-приложения
db = SQLAlchemy(app)

with app.app_context():
    # Создаем все таблицы в базе данных
    db.create_all()

# Запуск приложения
if __name__ == '__main__':
    app.run(debug=True)
