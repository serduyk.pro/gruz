from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from flask import Flask
import os

load_dotenv('.env')

# Создаем экземпляр Flask приложения
app = Flask(__name__)

# Определение класса конфигурации


class Config:
    # Секретный ключ для защиты форм и сессий
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_secret_key')

    # URI для подключения к базе данных
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL', 'sqlite:///app.db')

    # Отключение отслеживания изменений модели для SQLAlchemy
    SQLALCHEMY_TRACK_MODIFICATIONS = os.getenv(
        'SQLALCHEMY_TRACK_MODIFICATIONS', 'False')


# Применяем конфигурацию к приложению
app.config.from_object(Config)

# Инициализация SQLAlchemy после создания Flask-приложения
db = SQLAlchemy(app)

# Определение модели test


class Test(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    number = db.Column(db.String(15), nullable=False)
    mail = db.Column(db.String(100), nullable=True)
    adress1 = db.Column(db.Text, nullable=False)
    adress2 = db.Column(db.Text, nullable=False)

    def __repr__(self):
        return f"test('{self.name}', '{self.number}', '{self.adress1}', \
'{self.address2}')"
