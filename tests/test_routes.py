# tests/test_routes.py
import unittest
from app.route_all import app

class TestRoutes(unittest.TestCase):
    def setUp(self):
        # Создаем тестовый клиент Flask
        self.app = app.test_client()
        self.app.testing = True

    def test_index_route(self):
        # Проверяем доступность маршрута "/"
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)

    # def test_auth_route(self):
    #     # Проверяем доступность маршрута "/auth"
    #     response = self.app.get('/auth')
    #     self.assertEqual(response.status_code, 200)

    def test_index_route(self):
        # Проверяем доступность маршрута "/index"
        response = self.client.get('/index')
        self.assertEqual(response.status_code, 200)
        
    def test_index_route(self):
        # Проверяем доступность маршрута "/base"
        response = self.client.get('/base')
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
